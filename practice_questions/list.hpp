#ifndef LIST_HPP
#define LIST_HPP
#include <iostream>
#include <stdlib.h>
#include <time.h>

struct node{
  int data;
  node *next = NULL;
};

struct list{
  node *head = NULL;
  void add(int data);
  void print();
  void delete_node(node * n);
  void delete_multiples();
  node *find_k(int k);
  void part_about_x(int k);
  void push_front(int data);
  void pop_front();
  bool is_palindrome();
};

struct myQueue{
  stack ns;
  stack os;
  void push(int data);
  int pop();
  void newToOld();
}

struct stack{
  node * top = NULL;
  int pop();
  void push(int data);
  void print();
  void sort();
  int peek();
};

struct queue{
  node * first = NULL;
  node * last = NULL;
  int pop();
  void enqueue(int data);
  void print();
};

#endif
void list::add(int data){
  if(!head){
    node * tmp = new node();
    tmp->data = data;
    head = tmp;
    std::cout<<"\033[1;31m[LIST]\033[0m" <<"::Added " << data<< " to head" <<std::endl;
    return;
  }

  node * nnode = new node();
  nnode->data = data;
  node * tmp = head;
  int n = 0;
  while(tmp){
    n++;
    if(!tmp->next){
      tmp->next = nnode;
      std::cout<<"\033[1;31m[LIST]\033[0m" << "::Added " << data<< " to position " << n<<std::endl;
      break;
    }
    tmp=tmp->next;
  }
}

void myQueue::push(int data){

}

void list::push_front(int data){
  node * nnode = new node();
  nnode->data = data;
  node * tmp = head;
  head = nnode;
  nnode->next = tmp;
  return;
}
void list::delete_node(node *n){
  if(head == n){
    node * tmp = head;
    head = head->next;
    std::cout<<"\033[1;31m[LIST]\033[0m" << "::Deleted " << n->data<< " in position here " << 0<<std::endl;
    delete tmp;
    return;
  }
  node * tmp = head;

  int i = 1;
  while(tmp){
    if(tmp->next == n){
      tmp->next = tmp->next->next;
      std::cout<<"\033[1;31m[LIST]\033[0m" << "::Deleted " << n->data<< " in position " << i<<std::endl;
      delete n;
      break;
    }
    tmp=tmp->next;
    i++;
  }
}

void list::print(){
  node * tmp = head;
  while(tmp){
    std::cout<<tmp->data;
    tmp=tmp->next;
    if(tmp){
      std::cout<<"->";
    }
  }
  std::cout<<std::endl;
  return;
}

void list::delete_multiples(){
  bool history[10] = {false};
  node * tmp = head;
  while(tmp){
    if(history[tmp->data]==true){
      this->delete_node(tmp);
    }
    else{
      history[tmp->data]=true;
    }
    tmp=tmp->next;
  }

  return;
}

node *list::find_k(int k){
  node * tmp = head;
  node * kth = head;

  for(int i = 0;i<k;i++){
    if(tmp){
      tmp = tmp->next;
    }
    else{
      return NULL;
    }
  }

  while(tmp){
    kth=kth->next;
    tmp=tmp->next;
  }
  return kth;
}

void list::pop_front(){
  node * tmp = this->head;
  this->head = this->head->next;
  delete tmp;
  return;
}

bool list::is_palindrome(){
  list * palChecker = new list();
  node * first = head;
  node * second = head;
  bool foundEnd = false;
  while(first){
    if(!second || !second->next){
      foundEnd = true;
    }
    if(!foundEnd){
      palChecker->push_front(first->data);
    }
    else if(palChecker->head->data == first->data){
      palChecker->pop_front();
    }
    first=first->next;
    if(!foundEnd)
      second=second->next->next;
  }
  palChecker->print();
  if(palChecker->head && palChecker->head->next){
    return false;
  }
  return true;
}

void list::part_about_x(int x){
  node * tmp = head;
  bool found_x = false;
  while(tmp){
    if(tmp->data < x){
      this->push_front(tmp->data);
      node * del = tmp;
      tmp=tmp->next;
      this->delete_node(del);
      continue;
    }
    tmp=tmp->next;
  }
}

void stack::push(int data){
  node * n = new node();
  n->data = data;
  if(top == NULL){
    top=n;
    return;
  }
  node * tmp = top;
  top = n;
  n->next = tmp;
  return;
}

int stack::pop(){
  int data = top->data;
  node * tmp = top;
  top = top->next;
  delete tmp;
  return data;
}

int stack::peek(){
  return top->data;
}

void stack::print(){
  node * tmp = top;
  while(tmp){
    std::cout<<tmp->data;
    tmp=tmp->next;
    if(tmp){
      std::cout<<"->";
    }
  }
  std::cout<<std::endl;
  return;
}

void stack::sort(){
  stack s;

  if(s.top==NULL){
    s.push(this->pop());
  }

  while(this->top){
    std::cout<<"here"<<std::endl;

    int tmp = this->pop();
    while(s.top && s.peek() && tmp<s.peek()){
      this->push(s.pop());
    }
    s.push(tmp);
  }
  this->top = s.top;
}


void queue::enqueue(int data){
  node * n = new node();
  n->data = data;
  if(first == NULL){
    first = n;
    last = first;
    return;
  }
  last->next = n;
  last = last->next;
  return;
}

int queue::pop(){
  if(first == NULL){
    return -1;
  }
  node * tmp = first;
  first = first->next;
  int data = tmp->data;
  delete tmp;
  return data;
}

  void queue::print(){
    node * tmp = first;
    while(tmp){
      std::cout<<tmp->data;
      tmp=tmp->next;
      if(tmp){
        std::cout<<"->";
      }
    }
    std::cout<<std::endl;
    return;
  }

void sum(list l1, list l2){
  node * tmp1 = l1.head;
  node * tmp2 = l2.head;
  list * result = new list();
  int carry = 0;
  while(tmp1){
    int sum = tmp1->data + tmp2->data + carry;
    if(sum >= 10){
      result->add(sum%10);
      carry = 1;
    }
    else{
      result->add(sum);
      carry = 0;
    }
    tmp1=tmp1->next;
    tmp2=tmp2->next;
  }
  result->print();
}
