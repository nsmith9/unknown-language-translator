#include "list.hpp"

int main(){
  queue q;
  q.enqueue(1);
  q.enqueue(2);
  q.enqueue(3);
  q.enqueue(4);
  q.enqueue(5);
  q.pop();
  q.print();
  stack s;
  s.push(7);
  s.push(2);
  s.push(4);
  s.push(3);
  s.push(5);
  s.pop();
  s.print();
  s.sort();
  s.print();
  return 0;
}
