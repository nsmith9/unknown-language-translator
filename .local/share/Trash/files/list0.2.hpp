#ifndef LIST_HPP
#define LIST_HPP
#include <iostream>
#include <stdlib.h>
#include <time.h>

struct node{
  int data;
  node *next = NULL;
};

struct list{
  node *head = NULL;
  void add(int data);
  void print();
  void delete_node(node * n);
  void delete_multiples();
  node *find_k(int k);
  void part_about_x(int k);
  void push_front(int data);
  void pop_front();
  bool is_palindrome();
};
#endif
void list::add(int data){
  if(!head){
    node * tmp = new node();
    tmp->data = data;
    head = tmp;
    std::cout<<"\033[1;31m[LIST]\033[0m" <<"::Added " << data<< " to head" <<std::endl;
    return;
  }

  node * nnode = new node();
  nnode->data = data;
  node * tmp = head;
  int n = 0;
  while(tmp){
    n++;
    if(!tmp->next){
      tmp->next = nnode;
      std::cout<<"\033[1;31m[LIST]\033[0m" << "::Added " << data<< " to position " << n<<std::endl;
      break;
    }
    tmp=tmp->next;
  }
}

void list::push_front(int data){
  node * nnode = new node();
  nnode->data = data;
  node * tmp = head;
  head = nnode;
  nnode->next = tmp;
  return;
}
void list::delete_node(node *n){
  if(head == n){
    node * tmp = head;
    head = head->next;
    std::cout<<"\033[1;31m[LIST]\033[0m" << "::Deleted " << n->data<< " in position here " << 0<<std::endl;
    delete tmp;
    return;
  }
  node * tmp = head;

  int i = 1;
  while(tmp){
    if(tmp->next == n){
      tmp->next = tmp->next->next;
      std::cout<<"\033[1;31m[LIST]\033[0m" << "::Deleted " << n->data<< " in position " << i<<std::endl;
      delete n;
      break;
    }
    tmp=tmp->next;
    i++;
  }
}

void list::print(){
  node * tmp = head;
  while(tmp){
    std::cout<<tmp->data;
    tmp=tmp->next;
    if(tmp){
      std::cout<<"->";
    }
  }
  std::cout<<std::endl;
  return;
}

void list::delete_multiples(){
  bool history[10] = {false};
  node * tmp = head;
  while(tmp){
    if(history[tmp->data]==true){
      this->delete_node(tmp);
    }
    else{
      history[tmp->data]=true;
    }
    tmp=tmp->next;
  }

  return;
}

node *list::find_k(int k){
  node * tmp = head;
  node * kth = head;

  for(int i = 0;i<k;i++){
    if(tmp){
      tmp = tmp->next;
    }
    else{
      return NULL;
    }
  }

  while(tmp){
    kth=kth->next;
    tmp=tmp->next;
  }
  return kth;
}

void list::pop_front(){
  node * tmp = this->head;
  this->head = this->head->next;
  delete tmp;
  return;
}

bool list::is_palindrome(){
  list * palChecker = new list();
  node * first = head;
  node * second = head;
  bool foundEnd = false;
  while(first){
    if(!second || !second->next){
      foundEnd = true;
    }
    if(!foundEnd){
      palChecker->push_front(first->data);
    }
    else if(palChecker->head->data == first->data){
      palChecker->pop_front();
    }
    first=first->next;
    if(!foundEnd)
      second=second->next->next;
  }
  palChecker->print();
  if(palChecker->head && palChecker->head->next){
    return false;
  }
  return true;
}

void list::part_about_x(int x){
  node * tmp = head;
  bool found_x = false;
  while(tmp){
    if(tmp->data < x){
      this->push_front(tmp->data);
      node * del = tmp;
      tmp=tmp->next;
      this->delete_node(del);
      continue;
    }
    tmp=tmp->next;
  }
}

void sum(list l1, list l2){
  node * tmp1 = l1.head;
  node * tmp2 = l2.head;
  list * result = new list();
  int carry = 0;
  while(tmp1){
    int sum = tmp1->data + tmp2->data + carry;
    if(sum >= 10){
      result->add(sum%10);
      carry = 1;
    }
    else{
      result->add(sum);
      carry = 0;
    }
    tmp1=tmp1->next;
    tmp2=tmp2->next;
  }
  result->print();
}

int main(){
  // srand(time(NULL));
  // list * l = new list();
  // for(int i=0;i<10;i++){
  //   l->add(rand()%10);
  // }
  // l->print();
  // l->delete_multiples();
  // l->print();
  // node * kth = l->find_k(3);
  // std::cout<<kth->data<<std::endl;
  // l->part_about_x(5);
  // l->print();
  list l1;
  l1.add(7);
  l1.add(1);
  l1.add(6);
  l1.add(7);
  l1.add(0);
  l1.add(6);
  l1.add(1);
  l1.add(7);
  bool test = l1.is_palindrome();
  if(test)
    std::cout<<"is palindrome"<<std::endl;
  return 0;
}
